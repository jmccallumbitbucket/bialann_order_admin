﻿using System.Threading.Tasks;
using IdentityModel.OidcClient;

namespace BialannOrderAdmin
{
    public interface IAuthenticationService
    {
        Task<LoginResult> Authenticate();
        Task<IdentityModel.OidcClient.Browser.BrowserResultType> Logout();

    }
}