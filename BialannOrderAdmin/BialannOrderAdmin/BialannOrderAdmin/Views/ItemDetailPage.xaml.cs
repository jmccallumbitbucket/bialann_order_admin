﻿using BialannOrderAdmin.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace BialannOrderAdmin.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}